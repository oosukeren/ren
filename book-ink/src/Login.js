import React, { useEffect, useState } from "react";
import {useNavigate} from 'react-router-dom';
import eye from './pics/eye-icon2.png'

const Login = () => {
    const history = useNavigate();
    const [formValid, setValid] = useState(false)
    const [bool, toggle] = useState(false);
    const [form, setForm] = useState({
        email:{
            paragraph : "Email",
            value : "",
            type : "text",
            requirements : {
                min : 4,
                max : 40,
                matching : /\w/g
            },
            valid : false
        },
        password:{
            paragraph : "Password",
            value : "",
            type : "password",
            requirements : {
                min : 6,
                max : 20,
                matching : /[A-Za-z0-9]/g
            },
            valid : false
        }
    })

    const [par, setPar] = useState('');
    useEffect(()=>{
        setValid(form.email.valid && form.password.valid);
    },[form])

    useEffect(()=>{
        const type = bool?"text":"password";
        const password = {...form.password, type}
        const updatedForm = {...form, password}
        setForm(updatedForm)
    },[bool])
    const handleSubmit = event => {
        event.preventDefault()
        if(!formValid){
            setPar("Credentials are wrong!")
        } else {
            history('../', { replace: true });
            // const data = Object.keys(form)
            // .reduce((acc, elementKey) => {
            //     return {
            //         ...acc,
            //         [elementKey]: form[elementKey].value
            //     }
            // }, {})
            // const dataRes = JSON.stringify(data);
            // fetch(`${API}/user/login`,{
            //     mode: 'no-cors',
            //     method: 'POST',
            //     body: dataRes,
            //     headers: {
            //         'Content-Type':'application/json',
            //         'Content-Length':dataRes.length.toString(),
            //         'Host':'http://localhost:3000'
            //     }
            // })
            // .then(result => result.json())
            // .then((response)=> {
            //     if(response['token']){
            //         setPar('');
            //         localStorage.setItem('token',response['token']);
            //         history.push('/')
            //     }
            // })
            // .catch(a => {
            //     if(typeof a==="string"){
            //         setPar('Wrong email or password!')
            //     }
            // })
        }
    }
    const togglePasswordVisible = event => {
        event.preventDefault();
        toggle(!bool);
    }
    const validate = (obj, name) => {
        const matched = obj.value.split('').map(a => /[A-Za-z0-9]/.test(a)).reduce((a, b) => a && b,[true])
        if(!matched){
            if(obj.paragraph==="Email"){
                setPar('Allowed characters are only small, capital letters, numbers and underscore!');   
            } else {
                setPar('Allowed characters are only small, capital letters and numbers!');   
            }
        } else {
            setPar('');
        }
        const lengthReq = obj.value.length >= obj.requirements.min && obj.value.length <= obj.requirements.max;
        const valid  = matched && lengthReq;
        

        return valid;
    }
    const handleInputChange = event => {
        const {name, value} = event.target;
        const updatedElement = { ...form[name], value }
        const valid = validate(updatedElement, name);
        const updatedElement1 = {...updatedElement, valid};
        const updatedValidForm = {...form, [name]:updatedElement1}
        setForm(updatedValidForm)
    }

    const formElements = Object.keys(form)
    .map(name => {
        return{
            id: name,
            config: form[name]
        }
    })
    .map(({id, config})=>{
        return   <div key={id+'login2'} className="form-box" id={id+'-div'}>
                <label className="input-label" key={id+'login1'}>{config.paragraph}</label>
                <br></br>
                <input
                    className='login-input'
                    type={config.type}
                    key={id+'login'}
                    name={id}
                    value={config.value}
                    onChange={handleInputChange}
                />
                </div>
    })

    return <form className="login-screen" onSubmit={handleSubmit}>
            {formElements}
            <button type="submit" disabled={!formValid} className="login-button">LOG IN</button>
            <i onClick={togglePasswordVisible}> {bool?<img src={eye} id="eye" alt="" className="eye2"/>:<img src={eye} id="eye"  alt="" className="eye"/>} </i>
            <a href='/#' className="recover-password">Recover password</a>
            <p className="response">{par}</p>
            <p className="new-user-text">You don't have an account? <a href='/register' className="register-redirect">SIGN UP HERE</a></p>
    </form>
}

export default Login;