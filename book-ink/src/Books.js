import React from "react";
import { useState } from "react";
import logo from './pics/digi-logo.png';
import pfp from './pics/profile.png'
import magnify from './pics/Path 30139.png'
import { useLocation, useNavigate } from "react-router";
import SingleBookSearcResult from './SingleSearchResult'
import { useEffect } from "react/cjs/react.development";
export const SingleBookPage = () => {
    const [books, setBooks] = useState([
        {
            "_id": "618a75abefb465359670fd1c",
            "name": "Devolution",
            "author": "Max Brooks",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Devolution.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a7602efb465359670fd21",
                "name": "Horror"
            }
        },
        {
            "_id": "618a76ebefb465359670fd29",
            "name": "Filled with Fire and Light",
            "author": "Elie Wiesel",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Filled%20with%20Fire%20and%20Light.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a76aeefb465359670fd25",
                "name": "History"
            }
        },
        {
            "_id": "618a7789efb465359670fd2d",
            "name": "Harry Potter and the Goblet of Fire",
            "author": "Joanne Rowling",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Harry%20Potter.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "616743073b3d39971f0e94c6",
                "name": "Fantasy"
            }
        },
        {
            "_id": "618a78aeefb465359670fd3f",
            "name": "Little Women",
            "author": "Louisa May Alcott ",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Little%20Women.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "61671fea3b3d39971f0e94c4",
                "name": "Classic"
            }
        },
        {
            "_id": "618a7945efb465359670fd43",
            "name": "Midnight in Washington",
            "author": "Adam Schiff",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Midnight%20in%20Washington.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a76aeefb465359670fd25",
                "name": "History"
            }
        },
        {
            "_id": "618a7a20efb465359670fd4b",
            "name": "Rebel Homemaker",
            "author": "Drew Barrymore",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Rebel%20Homemaker.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a79bcefb465359670fd44",
                "name": "Biography"
            }
        },
        {
            "_id": "618a7b59efb465359670fd57",
            "name": "The Roommate",
            "author": "Rosie Danan",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/The%20Roommate.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a7a7befb465359670fd4f",
                "name": "Romance"
            }
        },
        {
            "_id": "618a7befefb465359670fd5e",
            "name": "Will",
            "author": "Will Smith",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Will.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a79bcefb465359670fd44",
                "name": "Biography"
            }
        }
    ])
    const location = useLocation();
    const query = location.search.split('=')[1];
    const book = books.filter(a=>a._id === query)[0];
    const d = book.createOn.split('T')[0];
    const d2 = book.lastUpdateOn.split('T')[0]
    return <div className="single-page">
            <img className="book-cover" src={book.image} alt="WhaT a nice looking repetitive book!"/>
            <div className="book-data">
                    <h1 className="single-book-title">{book.name}</h1>
                    <div className="book-data-container">
                        <p className="s-b-author">{book.author}</p>
                        <p id="single-book-data" className="s-b-genre">Genre: <b className="important">{book.genre.name}</b></p>
                        <p id="single-book-data" className="s-b-created">Created on: <b className="important">{d.split('-').join('.')}</b></p>
                        <p id="single-book-data" className="s-b-updated">Updated on: <b className="important">{d2.split('-').join('.')}</b></p>
                        <b className="important">Short description</b>
                        <p id="single-book-data">Harry Potter and the Goblet of Fire is a fantasy novel written by British author J. K. Rowling and the fourth novel in the Harry Potter series. It follows Harry Potter, a wizard in his fourth year at Hogwarts School of Witchcraft and Wizardry, and the mystery surrounding the entry of Harry’s name into the Triwizard Tournament, in which he is forced to compete.<br></br>The book was published in the United Kingdom by Bloomsbury and in the United States by Scholastic. In both countries, the release date was 8 July 2000. This was the first time a book in the series was published in both countries at the same time. The novel won a Hugo Award, the only Harry Potter novel to do so, in 2001. The book was adapted into a film, released worldwide on 18 November 2005, and a video game by Electronic Arts.</p>
                    </div>
            </div>
    </div>
}
const Library = () => {
    const [click, clicks] = useState(0);

    const [books, setBooks] = useState([
        {
            "_id": "618a75abefb465359670fd1c",
            "name": "Devolution",
            "author": "Max Brooks",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Devolution.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a7602efb465359670fd21",
                "name": "Horror"
            }
        },
        {
            "_id": "618a76ebefb465359670fd29",
            "name": "Filled with Fire and Light",
            "author": "Elie Wiesel",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Filled%20with%20Fire%20and%20Light.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a76aeefb465359670fd25",
                "name": "History"
            }
        },
        {
            "_id": "618a7789efb465359670fd2d",
            "name": "Harry Potter and the Goblet of Fire",
            "author": "Joanne Rowling",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Harry%20Potter.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "616743073b3d39971f0e94c6",
                "name": "Fantasy"
            }
        },
        {
            "_id": "618a78aeefb465359670fd3f",
            "name": "Little Women",
            "author": "Louisa May Alcott ",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Little%20Women.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "61671fea3b3d39971f0e94c4",
                "name": "Classic"
            }
        },
        {
            "_id": "618a7945efb465359670fd43",
            "name": "Midnight in Washington",
            "author": "Adam Schiff",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Midnight%20in%20Washington.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a76aeefb465359670fd25",
                "name": "History"
            }
        },
        {
            "_id": "618a7a20efb465359670fd4b",
            "name": "Rebel Homemaker",
            "author": "Drew Barrymore",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Rebel%20Homemaker.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a79bcefb465359670fd44",
                "name": "Biography"
            }
        },
        {
            "_id": "618a7b59efb465359670fd57",
            "name": "The Roommate",
            "author": "Rosie Danan",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/The%20Roommate.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a7a7befb465359670fd4f",
                "name": "Romance"
            }
        },
        {
            "_id": "618a7befefb465359670fd5e",
            "name": "Will",
            "author": "Will Smith",
            "image": "https://dev-fidweb.s3.eu-central-1.amazonaws.com/tasks/books/Will.jpeg",
            "createOn": "2021-10-09T21:00:00.000Z",
            "lastUpdateOn": "2021-10-09T21:00:00.000Z",
            "genre": {
                "_id": "618a79bcefb465359670fd44",
                "name": "Biography"
            }
        }
    ])
    const [results, setResults] = useState([]);
    useEffect(()=>{
        setResults(books.filter(a=>a.name.toLowerCase().includes(document.getElementById('search-bar').value.toLowerCase())))
    },[click])
    const sendQuery = event => {
        event.preventDefault()
        clicks(click+1);
    }

    return <div className="books-screen">
        <h1 className="books-screen-label">ALL BOOKS</h1>
        <input id="search-bar" type="text" className="search-bar" placeholder="Search"/>
        <img onClick={sendQuery} src={magnify} alt="" className="search-button"/>
        <div className="book-grid">
            {results.map(a=><SingleBookSearcResult key={a.id} a={a}/>)}
        </div>
    </div>
}
const Settings = () => {
    const [generalSettings, setGeneral] = useState(["NOTIFICATIONS AND EMAILS","USER MANAGEMENT","PHYSICAL LIBRARIES","READING EVENTS","INVOICING","BOOK STATISTICS","READERS STATISTICS","EVENTS STATISTICS"])
    const [bookSettings, setBook] = useState(["MANAGE GENRES","BOOK VISIBILITY","AUTHORS DATABASE","BOOK COVERS"])
    const split = (id) => {
        return id===1?
        generalSettings.map(a=>{
            return <div key={"split"+id+generalSettings.indexOf(a)} className="settings-row">
                <div className="left-end"><p className="settings-list-text" key={"text"+id+generalSettings.indexOf(a)}>{a}</p></div>
                <div className="line"></div>
            </div>
        })
        :
        bookSettings.map(a=>{
            return <div key={"split"+id+bookSettings.indexOf(a)} className="settings-row">
            <div className="left-end"><p className="settings-list-text" key={"text"+id+bookSettings.indexOf(a)}>{a}</p></div>
            <div className="line"></div>
        </div>
        })
    }
    return <div className="dual-settings">
        <div className="split1">
            <div className="settings-div1">
                <h1 className="settings-label">GENERAL SETTINGS</h1>
            </div>
            <div className="settings-list">
                {split(1)}
            </div>
        </div>
        <div className="split2">
                <div className="settings-div2">
                    <h1 className="settings-label">BOOK SETTINGS</h1>
                    <button className="settings-add-button">ADD NEW</button>
                </div>
                <div className="settings-list">
                    {split(2)}
                </div>

        </div>
    </div>
}
const Books = (path) => {

    return <div className="home-view">
        <div className="header">
            <img className="book-view-logo" src={logo} alt="What are you looking at?That's just a logo!"/>
            <a href='/library' id={path==="book"?"display1":"display2"} name="library" className="back-button">BACK TO LIBRARY</a>
            <a href='/library' id={path==="library"||path==="book"?"disabled-link":"active-link"} name="library" className="library-button">LIBRARY</a>
            <a href='/settings' id={path==="settings"?"disabled-link":"active-link"} name="settings" className="settings-button">SETTINGS</a>
            <img className="pfp" src={pfp} alt="Delete your browser history please!"></img>
        </div>
        {path==="library"?<Library/>:(path==="book"?<SingleBookPage/>:<Settings/>)}
    </div>

}

export default Books;