import React from "react";
import v1 from './pics/home_screen_pic/v1.png';
import logo from './pics/digi-logo.png';
import Login from "./Login";
import Register from "./Register";

const HomeScreen = (props) => {
    return <div className="home-screen">
        <img src={v1} className="home-screen-picture" alt='Ако четеш това наистина обръщаш огромно внимание на всички детайли от проекта ми!'/>
        <img src={logo} className="home-screen-logo" alt='Ако четеш това наистина обръщаш огромно внимание на всички детайли от проекта ми!'/>
        {props==="login"?<b className="login-title">WELCOME BACK!</b>:<p className="register-title">WELCOME TO THE BEST BOOK DATABASE!</p>}
        {props==="register"?<p className="register-title2">CREATE YOUR PROFILE</p>:""}
        {props==="login"?<Login/>:<Register/>}
    </div>
}

export default HomeScreen;