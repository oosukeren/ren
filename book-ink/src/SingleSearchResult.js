import React from "react";
import { useNavigate } from "react-router";
import playButton from './pics/Polygon 23.png';


const SingleBookSearcResult = (props) => {
    const a = props.a;
    const history = useNavigate()

    const goToSingleBookPage = event => {
        event.preventDefault()
        history(`../book?q=${a._id}`, { replace: true });
    }
    const d = a.createOn.split('T')[0];
    const d2 = a.lastUpdateOn.split('T')[0]
    return <div key={a._id+"placeholder"} className="book-placeholder">
                <img key={a._id+"image"} className="b-p-image" src={a.image} alt="i dunno"/>
                <div key={a._id+"data"} className="b-p-data">
                    <h1 key={a._id+"title"} className="b-p-title">{a.name.toUpperCase()}</h1>
                    <p key={a._id+"author"} className="b-p-author">{a.author}</p>
                    <p key={a._id+"genre"} className="b-p-genre">Genre: <b  key={a._id+"genre2"} className="b-p-genre-description">{a.genre.name}</b></p>
                    <div key={a._id+"date"} className="b-p-date">
                        <p key={a._id+"date2"} className="b-p-created">Created on: <b key={a._id+"date4"}  className="b-p-genre-description">{d.split('-').join('.')}</b></p>
                        <p key={a._id+"date3"} className="b-p-updated">Updated on: <b key={a._id+"date5"} className="b-p-genre-description">{d2.split('-').join('.')}</b></p>
                    </div>
                </div>
                <button key={a._id+"databutton"} onClick={goToSingleBookPage} name={a._id} className="single-book-redirect"><img src={playButton} alt="" className="single-book-redirect-icon"/></button>
            </div>
} 

export default SingleBookSearcResult;