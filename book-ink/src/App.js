import './App.css';
import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import HomeScreen from './Home';
import Books  from './Books';

function App() {
  const loggedIn = true;;
  
  return (
    <BrowserRouter>
        <Routes>
          <Route exact path="/" element={loggedIn?Books("library"):HomeScreen("register")}/>
          <Route exact path="/library" element={Books("library")}/>
          <Route exact path="/book" element={Books("book")}/>
          <Route exact path="/settings" element={Books("settings")}/>
          <Route exact path="/login" element={HomeScreen("login")}/>
          <Route exact path="/register" element={HomeScreen("register")}/>
        </Routes>
    </BrowserRouter>
  );
}

export default App;
